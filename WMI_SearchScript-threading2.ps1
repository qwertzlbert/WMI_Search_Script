﻿Function Get-WmiNamespace {
    Param (
        $Namespace='root'
    )
    Get-WmiObject -Namespace $Namespace -Class __NAMESPACE | ForEach-Object {
            ($ns = '{0}\{1}' -f $_.__NAMESPACE,$_.Name)
            Get-WmiNamespace $ns
    }
}

Function Get-RunspaceData {
    [cmdletbinding()]
    param(
        [switch]$Wait
    )
    Do {
        $more = $false         
        Foreach($runspace in $runspaces) {
            If ($runspace.Runspace.isCompleted) {

               #check if there were errors
               if($runspace.powershell.Streams.Error.Count -gt 0) {
               #set the logging info and move the file to completed
               foreach($ErrorRecord in $runspace.powershell.Streams.Error) {
                 Write-Error -ErrorRecord $ErrorRecord
                 }
                  }
                    else {
                       #add logging details and cleanup
                                Write-Verbose ("Completed")
                            }
                $runspace.powershell.EndInvoke($runspace.Runspace)
                $runspace.powershell.dispose()
                $runspace.Runspace = $null
                $runspace.powershell = $null                 
            } ElseIf ($runspace.Runspace -ne $null) {
                $more = $true
            }
        }
   
        #Clean out unused runspace jobs
        $temphash = $runspaces.clone()
        $temphash | Where {
            $_.runspace -eq $Null
        } | ForEach {
            Write-Verbose ("Removing {0}" -f $_.computer)
            $Runspaces.remove($_)
        }  
        if($PSBoundParameters['Wait']){ Start-Sleep -Milliseconds 100 }
            
    } while ($more -AND $PSBoundParameters['Wait'])
}


Function Find-WmiValue {
    [cmdletBinding()]
    param(
        [Parameter(Mandatory)]
        [String]$searchValue
    )
    
    $elapsedTime = [system.diagnostics.stopwatch]::StartNew()

    $namespaces = @('root/DEFAULT', 'root/CIMV2/power')
    #$namespaces= Get-WmiNamespace
    $outerPercentFactor=100 / $namespaces.length
    $outer=0
    
    $hits = [System.Collections.Generic.List[PsObject]]::new()
    
    $searchContainer = [hashtable](@{'searchValue' = $searchValue;})
    $statusContainer = [hashtable](@{})
    
    $maxThreads = 20

    $ScriptBlock = {
        Param ($searchContainer, $statusContainer, $hits)
       
        $classHits = Get-WmiObject -namespace $searchContainer.currentNamespace -class $searchContainer.currentClass | Where-Object {$_.psobject.properties.Value -like $searchContainer.searchValue}
        #$classHits = @{'namespace'= $searchcontainer.currentNamespace; 'Class' = $searchcontainer.currentClass; 'Search' = $searchContainer.searchvalue}
        Write-Progress -Activity Searching -Id 1 -Status "$([string]::Format("Time Elapsed: {0:d2}:{1:d2}:{2:d2}", $statusContainer.elapsedTime.Elapsed.hours, $statusContainer.elapsedTime.Elapsed.minutes, $statusContainer.elapsedTime.Elapsed.seconds))" -PercentComplete $($statusContainer.inner * $statusContainer.innerPercentFactor) -CurrentOperation "Search: $($searchContainer.currentClass)"

        Start-Sleep $(Get-Random -Maximum 15)

        if (-not [string]::IsNullOrEmpty($classHits)) {
        [System.Threading.Monitor]::Enter($hits.syncroot)
             [void]$hits.add($classHits)
        [System.Threading.Monitor]::Exit($hits.syncroot)
        }
        [System.Threading.Monitor]::Enter($statusContainer.syncroot)
             $statusContainer.inner++
        [System.Threading.Monitor]::Exit($statusContainer.syncroot)
        

    }

    $Script:runspaces = New-Object System.Collections.ArrayList   

    $sessionstate = [system.management.automation.runspaces.initialsessionstate]::CreateDefault()
    $runspacepool = [runspacefactory]::CreateRunspacePool(1, $maxThreads, $sessionstate, $Host)
    $runspacepool.Open() 

    $namespaces.ForEach{
        $currentNamespace=$_
        Write-Progress -Activity Searching -Status "$([string]::Format("Time Elapsed: {0:d2}:{1:d2}:{2:d2}", $elapsedTime.Elapsed.hours, $elapsedTime.Elapsed.minutes, $elapsedTime.Elapsed.seconds))" -PercentComplete $($outer * $outerPercentFactor) -CurrentOperation $currentNamespace
        $classes = Get-WmiObject -Namespace $_ -list
        #$classes = @(@{'Name' = 'Win32_Product'}, @{'Name' = 'Win32_VolumeQuota'}, @{'Name' = 'CIM_HostedBootSAP'}, @{'Name' = 'CIM_SystemDevice'})
        #$classes = @(@{'Name' = 'Sensor'}; @{'Name' = 'AdminDomain'})
        $innerPercentFactor=100 / $classes.length
        $statusContainer.inner=0
        $statusContainer.innerPercentFactor = $innerPercentFactor
        
        $classes.ForEach{
            $searchContainer.currentNamespace = $currentNamespace
            $statusContainer.elapsedTime = $elapsedTime
            $searchContainer.currentClass = $_.Name
            $currentClass=$_.Name
            $powershell = [powershell]::Create().AddScript($scriptBlock).AddArgument($searchContainer.clone()).AddArgument($statusContainer).AddArgument($hits)
            #Add the runspace into the powershell instance
            $powershell.RunspacePool = $runspacepool
           
            #Create a temporary collection for each runspace
            $temp = "" | Select-Object PowerShell,Runspace,Computer
            $temp.PowerShell = $powershell
           
            #Save the handle output when calling BeginInvoke() that will be used later to end the runspace
            $temp.Runspace = $powershell.BeginInvoke()
            $runspaces.Add($temp) | Out-Null  
        
        }
        Get-RunspaceData -Wait
        $outer++
    }
    Write-Verbose "$([string]::Format("Time to complete: {0:d2}:{1:d2}:{2:d2}", $elapsedTime.Elapsed.hours, $elapsedTime.Elapsed.minutes, $elapsedTime.Elapsed.seconds))"
    return $hits
}

